# Sonibble Web

<img src="assets/images/illustration-collabs.svg"/>

This project is an official website for sonibble. This website build using `Nuxt JS`, `Gitlab CI`, `NPM`.

### Background


<img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/25500248/logo1.png?width=64">

`Sonibble` are Creative Agency. We are come to help some people create their dream. With creative team will make something amazing and make our life easier.


### Contributing

If anyone want to contribute to become Sonibble creator, we're welcome. Consider to read the contribution rule [here](/CONTRIBUTING.md)

### Change Log

We're always update our product on Sonibble. To know the upadate please check all of the changelog in each version.
You can read [here](/CHANGELOG.md)

### WIKI

Consider to read the wiki of this project, You can find how to deploy, install, add some component for this project.

### License

Read the license of this project [here](/LICENSE.md)