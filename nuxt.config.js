export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Sonibble - Creative Dream Builder',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Build Your Own Dream With Creative Team Builder' },

      // twitter card seo tags
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        hid: 'twitter:site',
        name: 'twitter:site',
        content: '@sonibble'
      },
      {
        hid: 'twitter:creator',
        name: 'twitter:creator',
        content: '@nyomansunima'
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: 'Sonibble - Creative Dream Builder'
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'Build Your Own Dream With Creative Team Builder'
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'https://scontent.fcgk7-1.fna.fbcdn.net/v/t1.6435-9/129216946_404111054336553_6834111350168383674_n.png?_nc_cat=100&ccb=1-3&_nc_sid=e3f864&_nc_eui2=AeFo3cOPCG63z53sRjwcAw9OaiBhpx-PG3tqIGGnH48be5lUK-ARpt7-8sfbLrLQPINCDp4dgq94RU2nsCdTDnCD&_nc_ohc=BN0-UMdVVIcAX_mqxJR&_nc_ht=scontent.fcgk7-1.fna&oh=7fb68b26ab6228d204bdcd2372922d7e&oe=60BA5316'
      },
      {
        hid: 'twitter:image:alt',
        name: 'twitter:image:alt',
        content: 'Creative Dream Builder'
      },

      // facebook open graph seo tags
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Sonibble - Creative Dream Builder'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: 'Build Your Own Dream With Creative Team Builder'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://scontent.fcgk7-1.fna.fbcdn.net/v/t1.6435-9/129216946_404111054336553_6834111350168383674_n.png?_nc_cat=100&ccb=1-3&_nc_sid=e3f864&_nc_eui2=AeFo3cOPCG63z53sRjwcAw9OaiBhpx-PG3tqIGGnH48be5lUK-ARpt7-8sfbLrLQPINCDp4dgq94RU2nsCdTDnCD&_nc_ohc=BN0-UMdVVIcAX_mqxJR&_nc_ht=scontent.fcgk7-1.fna&oh=7fb68b26ab6228d204bdcd2372922d7e&oe=60BA5316'
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: 'https://scontent.fcgk7-1.fna.fbcdn.net/v/t1.6435-9/129216946_404111054336553_6834111350168383674_n.png?_nc_cat=100&ccb=1-3&_nc_sid=e3f864&_nc_eui2=AeFo3cOPCG63z53sRjwcAw9OaiBhpx-PG3tqIGGnH48be5lUK-ARpt7-8sfbLrLQPINCDp4dgq94RU2nsCdTDnCD&_nc_ohc=BN0-UMdVVIcAX_mqxJR&_nc_ht=scontent.fcgk7-1.fna&oh=7fb68b26ab6228d204bdcd2372922d7e&oe=60BA5316'
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: 'Creative Dream Builder'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/awesome.css',
    'jam-icons/css/jam.min.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // the aos plugins
    { src: "~/plugins/aos", mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components/headers',
      '~/components/footers',
      '~/components/cards'
    ]
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  // define the rendering type this can be 'static' or 'server'
  target: 'static'
}
